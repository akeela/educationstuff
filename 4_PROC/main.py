#!/usr/bin/env python3
#using: utf-8

import os, sys

def main():
    try:
        if (len(sys.argv)<=1):
            return
        print("Process parent ID:", os.getpid())
        pid = []
        i = j = 0
        while (i < int(sys.argv[1])):
            pid.append(os.fork())
            if (not pid[i]):
                os.execl("ball.py","ball.py")
            i += 1
        for j in pid:
            os.waitpid(j, 0)

    except Exception as e:
        print (e)

if __name__ == "__main__":
    main()
    print("exit")
