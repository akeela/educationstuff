/*
    Координаты заданного количества шариков изменяются на случайную величину по
    вертикали и горизонтали, при выпадении шарика за нижнюю границу допустимой области
    шарик исчезает. Напишите программу изменения координат одного шарика и программу,
    создающую для каждого из заданного количества шариков порожденный процесс изменения
    их координат.
*/

// Linux Proc

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

int main (int argc, char* argv[])
{
    if (argc!=2)
        return -1;

    int count = strtol(argv[1],NULL,10);
    printf("pid: %d\n", getpid());

    int ret, status;
    pid_t num, wpid;
    pid_t children[count];
    int i;
    for (i = 0; i < count; i++)
    {
        status = 0;
        children[i] = fork();
        sleep(1);
        if (!children[i])
        {
            ret = execl("ball", "ball", NULL);
            if (ret == -1)
                printf("error_execl");
        }
    }

    for(int j = 0; j < i; j++){
        waitpid(children[j], NULL, 0);
    }

    //while ((wpid = wait(&status)) > 0);
    printf("exit\n");

    return 0;
}
