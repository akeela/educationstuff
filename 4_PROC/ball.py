#!/usr/bin/env python3
#using: utf-8

import os, random, time

DEATH = 10
print("PPID:{}".format(os.getppid()))
while(1):
    time.sleep(1)
    ver = random.randint(0, 99)
    hor = random.randint(0, 99)
    if (ver < DEATH or hor < DEATH):
        print("{}: ({},{}) - ball destroyed!".format(os.getpid(),ver,hor))
        break
    print("{}: ({},{})".format(os.getpid(),ver,hor))
