// ball

#define DEATH 5

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

#include <signal.h>
#include <stdlib.h>

int main (int argc, char* argv[])
{
    srand(time(0));

    int mypid = getpid();
    int myppid = getppid();
    int ver, hor; 
    printf("PPID: %u\n",myppid);
    while (1)
    {
        sleep(1);
        ver = rand() % 99 + 1;
        hor = rand() % 99 + 1;
        if (ver < DEATH || hor < DEATH)
        {
            printf("%u: (%u,%u) - ball destroyed!\n", mypid, ver, hor);
            exit(0);
        }
        printf("%u: (%u,%u)\n", mypid, ver, hor);
    }
    
    return 0;
}
