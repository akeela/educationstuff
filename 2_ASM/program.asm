%include "io64.inc"

; дан массив из 10 слов. Инвертировать все 
; отрицательные числа и найти сумму элементов 
; полученного массива

%define STEP 2

section .data
        	message: dd "Negative",0xA ; 0xA new line
        	messageLen: equ $-message ; somehow считает длинну
        
	array: dw 65,-66,67,68,69,70,71,72,-73,74
	arrayLen: equ ($-array)/2

section .text
global CMAIN
CMAIN:
        mov rbp, rsp; for correct debugging

        mov rsi, array ; указатель на массив
        mov rdi, arrayLen ; кол-во элементов в массиве
        
        ; SHOW ARRAY
        mov rax, 4 ; write
        mov rbx, 1 ; STDOUT
        mov rcx, rsi
        mov rdx, arrayLen*2
        int 0x80
        
        mov rbx, 0 ; Номер элемента в массиве
        mov rcx, 0
        mov rax, 0 ; Сумма всех элементов
        
COMPARE:
        cmp rbx, rdi ; while (rbx != rdi)
        jge QUIT
        
        mov cx, [rsi+STEP*rbx]
        cmp cx, 0
        jl MAKEPOSITIVE; переход если меньше (со знаком)
        inc rbx ; i
        add rax, rcx
        jmp COMPARE

MAKEPOSITIVE:
        xor cx,0xffff
        inc cx
        mov [rsi+STEP*rbx], cx
          
        add rax, rcx
        inc rbx ; i
        
        mov rcx, 0
        jmp COMPARE

QUIT:
        push rax ;save summ
        
        ; SHOW ARRAY
        mov rax, 4
        mov rbx, 1
        mov rcx, rsi
        mov rdx, arrayLen*2
        int 0x80
        
        pop rax
        mov rcx, rax

        ; SHOW SUMM
        ;mov rax, 4
        ;mov rbx, 1
        ;mov rcx, rsi
        ;mov rdx, 2
        ;int 0x80
        
        mov rax, 1
        mov rbx, 0
        int 0x80