#include <stdio.h>

int main()
{
    int arr[] = {65,-66,67,68,69,70,71,72,-73,74,75};
    int len = sizeof(arr)/sizeof(arr[0]);
    int sum = 0;

    for (int i = 0; i < len ; ++i)
        printf("%c", arr[i]);
    for (int i = 0; i < len ; ++i)
    {
        if (arr[i] < 0)
        {
            arr[i] = ~arr[i] + 1;
        }
        sum += arr[i];
        printf("%c", arr[i]);
    }

    printf("\n%u\n", sum);

    return 0;
}
