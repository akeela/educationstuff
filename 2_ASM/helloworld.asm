; дан массив из 10 слов. Инвертировать все 
; отрицательные числа и найти сумму элементов 
; полученного массива

section .text
	global _start

section .data
	message: dd "Hello there",0xA ; 0xA new line
	messageLen: equ $-message ; somehow считает длинну

_start:
	mov eax, 4 ; __NR_write
	mov ebx, 1 ; param = STDOUT
	mov ecx, message ;lea ecx, array ; 1
	mov edx, messageLen; Написать * знаков
	int 80h ; syscall	ret
	
	mov eax, 1
	mov ebx, 0
	int 80h
