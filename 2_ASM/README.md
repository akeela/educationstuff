# Compile
nasm -f elf64 main.asm -o hello.o

# Link
ld hello.o -o hello
or
gcc -g -o program program.o -m64 -fno-pie -no-pie

