// posix, i use STD instead
//#include <unistd.h>
//#include <pthread.h>
#include <iostream>
#include <string>

#include <vector>
#include <thread>
#include <future>

#include <ctime>
#include <random>

const int DEATH = 10; 

int thread_ret(int num)
{
    return num;
}

int thread_ball(int tnum)
{
    int ver, hor;
    while (1)
    {
        std::srand(std::time(0));
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        ver = std::rand() % 99;
        hor = std::rand() % 99;
        
        if (ver < DEATH || hor < DEATH)
        {
            std::cout << std::this_thread::get_id() << " " << ver << " : " << hor << " - ball destroyed!" << std::endl;
            return (ver*100+hor);
        }
        std::cout << std::this_thread::get_id() << " " << ver << " : " << hor << std::endl;
    }
}

int main(int argc, char* argv[])
{
    if (argc!=2)
        return -1;
    
    int count = std::stoi(argv[1],NULL,10);

    std::vector<std::future<int>> futures;
    for(int i = 0; i < count; ++i) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        futures.push_back (std::async(thread_ball, i));
    }

    for(auto &e : futures) {
        std::cout << e.get() << std::endl;
    }

    return 0;
}