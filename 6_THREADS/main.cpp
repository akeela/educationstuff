// posix, i use STD instead
//#include <unistd.h>
//#include <pthread.h>
#include <iostream>
#include <string>

#include <vector>
#include <thread>
#include <future>

#include <ctime>
#include <random>

#include <list>

const int DEATH = 10; 

std::list <std::string> mylist;
std::mutex mymutex;

int thread_ball(int tnum)
{
    int ver, hor;
    std::string str = "";
    while (1)
    {
        std::srand(std::time(0));
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        ver = std::rand() % 99;
        hor = std::rand() % 99;
        str = std::to_string(tnum) + " " + std::to_string(ver) + " : " + std::to_string(hor);
        // Работа с критической областью
        std::lock_guard<std::mutex> lock(mymutex);
        if (ver < DEATH || hor < DEATH)
        {
            str += " - ball destroyed! \n";
            mylist.push_back(str);
            return (ver*100+hor);
        }
        str += "\n";
        mylist.push_back(str);
        // Работа с критической областью
    }
}

int main(int argc, char* argv[])
{
    if (argc!=2)
        return -1;
    int count = std::stoi(argv[1],NULL,10);

    std::vector<std::future<int>> futures;
    for(int i = 0; i < count; ++i) {
        //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        futures.push_back (std::async(thread_ball, i));
    }
    for(auto &e : futures) {
        std::cout << e.get() << std::endl;
    }

    for (auto const& i: mylist) {
		std::cout << i;
	}

    return 0;
}