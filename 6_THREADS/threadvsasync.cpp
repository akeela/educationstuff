#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <future>
 
int main(int argc, char* argv[])
{
   int res;
   // потоки
   std::thread t([&]{res= 2*2;});
   t.join();
   std::cout << res << std::endl;
  
   // будущее
   auto fut=std::async([]{return 2*2;});
   std::cout << fut.get() << std::endl;
 
   return 0;
}
