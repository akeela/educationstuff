#!/usr/bin/env bash

echo "Включение репозиторий"
echo "Беляков Данила"
echo

for ((;;))
do
    yum repolist all
    echo "Введите название репозитория"
    read repo
    if (yum repolist all | grep enable) | grep $repo
    then
	    echo "Найден (выключаем)"
            yum-config-manager --disable $repo
    elif (yum repolist all | grep disable) | grep $repo
    then
	    echo "Найден (включаем)"
            yum-config-manager --enable $repo
    else
            echo "Репозиторий не найден"
    fi
    read -p "Хотите продолжить [y/n]" cont
    if [[ $cont == "n" ]]
    then
	    break
    fi
done
echo "have a nice day :3"
