//============================================================================
// Author      : Danila Belykov
// Напишите программу, в которой создается квадратная матрица.
// Матрица заполняется случайными числами, после чего выполняется
// «поворот по часовой стрелке»: первый столбец становится первой
// строкой, второй столбец становится второй строкой, и так далее.
//============================================================================


#include <cstdio>
#include <iostream>
#include <ctime>

using namespace std;

#define SIZE 4

int main() {

	srand(time(0));
	long long n = SIZE;

	long long array[n][n] = {
			{1,2,3,4},
			{5,6,7,8},
			{9,10,11,12},
			{13,14,15,16}
	};

	//long long array[n][n];
	long long result[n][n];
/*
	for (int i = 0; i < n; i++)
		{
		for (int j = 0; j < n; j++)
		{
			array[i][j] = rand() % 9 + 1;
			//cout<<array[i][j]<<" ";
		}
		cout<<"\n";
	}
*/
	long long *ptr_array = &array[0][0];
	long long *ptr_result = &result[0][0];

	__asm__(
			"push %%rbx"				"\n\t"
			"push %%rsp"				"\n\t"
			"push %%rbp"				"\n\t"
			//инициализация указателей регистры
			"mov %[ptr_array], %%rsi" 	"\n\t" // A
			"mov %[ptr_result], %%rdi" 	"\n\t" // B
			"mov $0, %%rax"			"\n\t" // 0
			"mov %[n], %%rax" 		"\n\t" // n = 4
			"mul %%rax" 			"\n\t" // 16
			// rcx регистр счётчик (для loop)
			"mov %%rax, %%rcx" 		"\n\t"	// rcx = 9

			"sub %[n], %%rax"		"\n\t"	// 9-3=6
			"movq $8, %%rdx"		"\n\t"
			"mul %%rdx"				"\n\t"	// 6*8
			// rbx регистр смещение
			"mov %%rax, %%rbx" 		"\n\t" 	// rbx = 48 смешение (9-3=6)

			"ROT:" 					"\n\t"
			"push %%rsi"			"\n\t"	// Сохраняю адрес первого элемента

			"add %%rbx, %%rsi"		"\n\t"	// смешение в 1 матрицы на +6

			//вычисляю след. смешение
			"mov %[n], %%rax"		"\n\t"	// 3
			"movq $8, %%rdx"		"\n\t"	// 3*8
			"mul %%rdx"				"\n\t"	// 3*8=24
			"sub %%rax, %%rbx"		"\n\t" 	// rbx-3 (6-3=3)

			"cmp $0, %%rbx"			"\n\t"	// rbx >= 0
			"jge POS"				"\n\t"

			"mov %[n], %%rax"		"\n\t" 	// 3
			"mul %%rax"				"\n\t"	// 9
			"movq $8, %%rdx"		"\n\t"	//
			"mul %%rdx"				"\n\t"	// 9*8=72
			"add %%rax, %%rbx"		"\n\t"	// ПРИБАВЛЯЮ к rbx += 72 (+9)
			"add $8, %%rbx"			"\n\t"	// rbx+=80 (+10)

			"POS:" 					"\n\t"
			"mov (%%rsi),%%rax"		"\n\t"	// Беру значение из 1 матрицы
			"mov %%rax,(%%rdi)"		"\n\t"	// Записываю значение во 2 матрицу
			"add $8, %%rdi"			"\n\t"	// Перемещаю указатель 2 матрицы на след. эл.

			"pop %%rsi"				"\n\t"	// Возвращаю указатель на 1 элемент 1 матрицы
			"loop ROT" 				"\n\t"

			"pop %%rbp"				"\n\t"
			"pop %%rsp"				"\n\t"
			"pop %%rbx"				"\n\t"
			"end_asm:" 				"\n\t"
			: // выходные параметры
			:[ptr_array]"m"(ptr_array),[ptr_result]"m"(ptr_result),[n]"m"(n) // входные параметры
			:"%rax", "%rbx", "%rcx"	// разрушаемые регистры
	);

	cout<<"\nresult:\n";
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			cout<<result[i][j]<<" ";
		cout<<"\n";
	}
	return 0;
}

