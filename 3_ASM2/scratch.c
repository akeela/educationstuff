//============================================================================
// Author      : Danila Belykov
// Напишите программу, в которой создается квадратная матрица.
// Матрица заполняется случайными числами, после чего выполняется
// «поворот по часовой стрелке»: первый столбец становится первой
// строкой, второй столбец становится второй строкой, и так далее.
//============================================================================


#include <cstdio>
#include <iostream>
#include <ctime>

using namespace std;

#define SIZE 4

extern "C"{
	void rotateC(int array[][SIZE]);
}


int main() {
	srand(time(0));
	int n = SIZE;

	long long array[n][n] = {
			{1,2,3,4},
			{5,6,7,8},
			{9,10,11,12},
			{13,14,15,16}
	};
	long long result[n][n]; // result array
	/*
	for (int i = 0; i < n; i++)
		{
		for (int j = 0; j < n; j++)
		{
			array[i][j] = rand() % 9 + 1;
			cout<<array[i][j]<<" ";
		}
		cout<<"\n";
	}
	*/
	long long *ptr_array = &array[0][0];
	long long *end_array = &array[n-1][n-1];

	long long *ptr_result = &result[0][0];
	long long *end_result = &result[n-1][n-1];

	//printf("%u\n", end_array-ptr_array);

	//"cmp %%rax, %[end_array]\n\t" // Проверили что достигнут конец
	__asm__(
		"push %%rdx" "\n\t"
		//"mov $1, %%rcx" "\n\t" //смещение массива
		"mov %[_aptr], %%rbx" "\n\t" //начало
		//"mov %[_aend], %%rax" "\n\t" //конец массива

		//находим кол-во всего элементов
		"sub %%rbx, %%rax" "\n\t" //всего элементов :8
		"mov $0, %%rdx" "\n\t"
		"mov $8, %%rbx" "\n\t"
		"div %%rbx" "\n\t"
		"mov %%rax, %%rcx" "\n\t" //rcx = 15
		"div %%rbx" "\n\t"
		//"inc %%rax" "\n\t" //rax = 16 (при 4x4)
		"LOOP:"
		"cmp $0, %%rcx" "\n\t"
		"je QUIT" "\n\t"
		"sub $1, %%rcx" "\n\t"
		//do smth
		"" "\n\t"
		"jmp LOOP" "\n\t"

		//"mov (%%rax), %%rcx" "\n\t"
		//"add $(8*1), %%rax" "\n\t"
		//"mov %[_aend] - %[_aptr], %%rcx" "\n\t"
		"QUIT:" "\n\t"
		"nop" "\n\t"
		"pop %%rdx" "\n\t"
		"end_asm:" "\n\t"
		:
		:[_aptr]"m"(ptr_array),[_rptr]"m"(ptr_result),[n]"m"(n)
		:"%rax", "%rbx", "%rcx"
	);

	cout<<"\nresult:\n";
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			cout<<array[i][j]<<" ";
		cout<<"\n";
	}
	//rotateC(a);
	return 0;
}

void rotateC(int array[][SIZE])
{
    for (int x = 0; x < SIZE / 2; x++) {
        for (int y = x; y < SIZE - x - 1; y++) {
            int temp = array[x][y];
            array[x][y] = array[SIZE - 1 - y][x];
            array[SIZE - 1 - y][x] = array[SIZE - 1 - x][SIZE - 1 - y];
            array[SIZE - 1 - x][SIZE - 1 - y] = array[y][SIZE - 1 - x];
            array[y][SIZE - 1 - x] = temp;
        }
    }
}



