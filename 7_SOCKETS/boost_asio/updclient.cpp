#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;

int main()
{
    //boost::asio::io_context io_context; 
    boost::asio::io_service io_ser;
    std::string msg = "hello from client\n";
    char buff[512] = {}; // for recv data from server (answer)
    try
    {
        ip::udp::socket socc( io_ser );
        ip::udp::endpoint remote( ip::address::from_string("127.0.0.1"), 44401 ); 
        socc.open( ip::udp::v4() );

        socc.send_to( boost::asio::buffer(msg), remote );
        //read answer
        socc.receive_from( boost::asio::buffer(buff), remote );
        std::cout << buff << '\n';
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}