#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;

int main()
{
    boost::asio::io_service io_ser;
    char buff[512] = {};
    try
    {
        ip::udp::endpoint client( ip::udp::v4(), 44401);
        ip::udp::socket socc( io_ser, client );

        socc.receive_from( boost::asio::buffer(buff), client );
        std::cout << buff << '\n';
        socc.send_to( boost::asio::buffer("hello from server"), client );
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    
    return 0;
}