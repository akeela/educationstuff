#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;

int main()
{
    char buff[512];
    boost::asio::io_service io_ser;
    
    try
    {
        ip::tcp::socket socc(io_ser);

        socc.connect( ip::tcp::endpoint(
            ip::address::from_string("127.0.0.1"), 44400));
        std::string msg = "hello from client\n";

        boost::asio::write( socc, boost::asio::buffer(msg));

        boost::asio::streambuf receive_buf;
        socc.read_some(buffer(buff));
        std::cout << buff << std::endl;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}