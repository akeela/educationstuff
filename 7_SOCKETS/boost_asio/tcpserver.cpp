#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;

int main()
{
    char buff[512]; 
    boost::asio::io_service io_ser;
    try
    {
        ip::tcp::acceptor myacceptor( io_ser, ip::tcp::endpoint(
            ip::tcp::v4(), 44400 ));
        ip::tcp::socket socc( io_ser );

        myacceptor.accept( socc );

        socc.read_some(buffer(buff));
        std::cout << buff << std::endl;

        boost::asio::write( socc, boost::asio::buffer("hello from server"));
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    return 0;
}