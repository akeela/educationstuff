//============================================================================
// Author      : Danila Belykov
/* 	Напишите программу, в которой создается двумерный числовой массив. Массив
	заполняется случайными числами. На его основе создается новый массив, который получается
	«вычеркиванием» из старого одной строки и одного столбца. Номера «вычеркиваемых» столбца
	и строки определяются вводом с клавиатуры или с помощью генератора случайных чисел.
*/
//============================================================================


#include <cstdio>
#include <iostream>
#include <ctime>

using namespace std;

#define SIZE 3

int main() {

	srand(time(0));
	long long n = SIZE;

	long long array[n][n] = {
			{1,2,3},
			{4,5,6},
			{7,8,9}
	};
	// Считаем с 1!!!!
	long long x = 1; //0.X
	long long y = 3; //X.0


	//long long array[n][n];
	long long result_func[n-1][n-1];
	long long result[n][n];

	for (int i = 0; i < n; i++)
		{
		for (int j = 0; j < n; j++)
		{
			//array[i][j] = rand() % 9 + 1;
			cout<<array[i][j]<<" ";
		}
		cout<<"\n";
	}
	/*
	int l = y-1;
	int k = x-1;

	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - 1; j++)
		{
			printf("[%u,%u]=%u \n", i, j, array[i][j]);
			if ( i < y )
				if ( j < x )
					result_func[i][j] = array[i][j];
				else
					result_func[i][j] = array[i][j + 1];
			else
				if ( j < x )
					result_func[i][j] = array[i + 1][j];
				else
					result_func[i][j] = array[i + 1][j + 1];
		}
	}
*/

	long long *ptr_array = &array[0][0];
	long long *ptr_result = &result[0][0];

	__asm__(
			"push %%rbx"				"\n\t"
			"push %%rsp"				"\n\t"
			"push %%rbp"				"\n\t"
			//инициализация указателей регистры
			"mov %[ptr_array], %%rsi" 	"\n\t" // A
			"mov %[ptr_result], %%rdi" 	"\n\t" // B

			"mov $0, %%rcx"				"\n\t" // 0
			"mov %[n], %%rax"			"\n\t" // n = 3
			"mov %[n], %%rcx"			"\n\t" // n = 3
			"mul %%rax"					"\n\t" // n = 9
			"sub %%rcx, %%rax"			"\n\t"
			"mov %%rax, %%rcx" 			"\n\t" // n = 6


			//"dec %%rcx"					"\n\t" // n -1 = 2
			"mov $0, %%rbx"				"\n\t"
			"mov %[y], %%rbx"			"\n\t" // rbx = 2

			"LOOPER:" 				"\n\t"

			"cmp $1, %%rbx"				"\n\t" // 1 > rbx то переход
			"jne SWAPFUNC"				"\n\t"
			//"jl SWAPFUNC"				"\n\t"
			"mov %[n], %%rax"			"\n\t" // rax = 3
			"movq $8, %%rdx"			"\n\t"
			"mul %%rdx"					"\n\t"
			"add %%rax, %%rsi"			"\n\t" // смешение rsi = 4 + 3

			"SWAPFUNC:" 					"\n\t"

			"push %%rcx"				"\n\t"
			"movq $3, %%rcx"			"\n\t"
			"SWAP3:"					"\n\t"
			"mov (%%rsi),%%rax"			"\n\t" // замена
			"mov %%rax,(%%rdi)"			"\n\t"
			"add $8, %%rdi"				"\n\t"
			"add $8, %%rsi"				"\n\t"
			"loop SWAP3"				"\n\t"

			"pop %%rcx"					"\n\t"
			"dec %%rbx"					"\n\t"


			"loop LOOPER"				"\n\t"

			"pop %%rbp"				"\n\t"
			"pop %%rsp"				"\n\t"
			"pop %%rbx"				"\n\t"
			"end_asm:" 				"\n\t"
			: // выходные параметры
			:[ptr_array]"m"(ptr_array),[ptr_result]"m"(ptr_result),[n]"m"(n),[y]"m"(y),[x]"m"(x) // входные параметры
			:"%rax", "%rbx", "%rcx"	// разрушаемые регистры
	);
/**/
	cout<<"\nresult:\n";
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			cout<<result[i][j]<<" ";
		cout<<"\n";
	}


	cout<<"\nresult_func:\n";
	for (int i = 0; i < n-1; i++)
	{
		for (int j = 0; j < n-1; j++)
			cout<<result_func[i][j]<<" ";
		cout<<"\n";
	}
	return 0;
}




